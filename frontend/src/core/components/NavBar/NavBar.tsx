import { NavLink } from 'react-router-dom';
import './styles.scss';

const NavBar = () => {
    return (
        <nav className='navbar-container'>
            <ul>
                <li>
                    <NavLink to='/'>
                        home
                    </NavLink>
                </li>
                <li>
                    <NavLink to='/game'>
                        jogar
                    </NavLink>
                    
                </li>
                <li>
                    <NavLink to='/ranking'>
                        ranking
                    </NavLink>
                </li>
            </ul>
        </nav>
    );
}

export default NavBar;