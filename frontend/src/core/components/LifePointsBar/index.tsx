//import './styles.scss';

type Props = {
    name: string;
    life: number;
}

const LifeBarColor = (value: number) => {
    switch(true) {
        case (value >= 50):
             return '#b5d23b';
        case  (value < 50 && value >= 20):
            return '#FBFF20';
        case (value < 20):
            return '#DC2525';        
    }
}

const LifePointsBar = ({ name, life }: Props) => {

    return (
        <div className='life-point-content'>
            <h4>{name}</h4>
            <div className="life-points-bar">
                <h5>
                    {life}%
                </h5>
                <div
                    style={{
                        width: `${life}%`,
                        height: '100%',
                        backgroundColor: LifeBarColor(life)
                    }}
                />
            </div>
        </div>
    );
}

export default LifePointsBar;