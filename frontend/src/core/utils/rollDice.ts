const rollDice = (diceValue: number, minimumValue: number) => {
    const result = Math.ceil(Math.random() * diceValue) + minimumValue;

    return result;
}

export const PlayerBasicAttack = () => {
    // entre 5 e 10
    //1d6 + 4, pois o minimo valor do dado eh 1
    const result = rollDice(6, 4);

    return result;
}

export const PlayerEspecialAttack = () => {
    // entre 10 e 20
    //1d11 + 9
    const result = rollDice(11, 9);

    return result;
}

export const PlayerHeal = () => {
     // entre 5 e 15
    //1d11 + 4
    const result = rollDice(11, 4);

    return result;
}

export const EnemyBasicAttack = () => {
    // entre 6 e 12
    //1d7 + 5
    const result = rollDice(7, 5);

    return result;
}

export const EnemyEspecialAttack = () => {
    // entre 8 e 16
    //1d9 + 7
    const result = rollDice(9, 7);

    return result;
}

export const EnemyGetSturn = () => {
    const result = Math.round(Math.random());
    // if (result)
    //     console.log("Inimigo atordoado!");
    return Boolean(result);
} 