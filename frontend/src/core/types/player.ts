export type PlayersResponse = {
    content: Player[],
    size: number,
    page: number,
    totalPages: number,
    totalElements: number
}

export type Player = {
    _id: number,
    playerName: string,
    data: Date,
    score: number
}