import NavBar from "core/components/NavBar/NavBar";
import GamePage from "pages/GamePage";
import HomePage from "pages/HomePage";
import RankingPage from "pages/RankingPage";
import { BrowserRouter, Route, Routes } from "react-router-dom";

const Screens = () => {
    return (
        <BrowserRouter>
            <NavBar />
            <Routes>
                <Route path="/" element={<HomePage />} />
                <Route path="/game" element={<GamePage />}/>
                <Route path="/ranking" element={<RankingPage />}/>
            </Routes>
        </BrowserRouter>
    );
}

export default Screens;