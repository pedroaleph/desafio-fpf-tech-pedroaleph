import LifePointsBar from 'core/components/LifePointsBar';
import { Player } from 'core/types/player';
import { formatDate } from 'core/utils/format';
import request from 'core/utils/request';
import {
    EnemyBasicAttack, EnemyEspecialAttack, EnemyGetSturn, PlayerBasicAttack, PlayerEspecialAttack, PlayerHeal
} from 'core/utils/rollDice';
import { useState } from 'react';
import { Link } from 'react-router-dom';
import './styles.scss';

const GamePage = () => {
    const [isGameActive, setIsGameActive] = useState(false);
    const [playerLife, setPlayerLife] = useState(100);
    const [enemyLife, setEnemyLife] = useState(100);
    const [playerCoolDown, setPlayerCoolDown] = useState(0);
    const [enemyCoolDown, setEnemyCoolDown] = useState(3);
    const [isEnemyStunned, setIsEnemyStunned] = useState(false);
    const [log, setLog] = useState<string[]>([]);
    const [turn, setTurn] = useState(1);
    const [playerName, setPlayerName] = useState('');
    const [player, setPlayer] = useState<Player>();

    const OnHandleGiveUp = () => {
        const confirm = window.confirm("Deseja realmente desistir?");

        if (confirm) {
            setIsGameActive(false);
            setPlayerLife(100);
            setEnemyLife(100);
            setPlayerCoolDown(0);
            setEnemyCoolDown(3);
            setIsEnemyStunned(false);
            setLog([]);
            setTurn(1);
        }
    }

    const OnHandlePlayerCoolDown = () => {
        if (playerCoolDown > 0)
            setPlayerCoolDown(playerCoolDown - 1);
    }

    const OnHandleEnemyCoolDown = () => {
        if (enemyCoolDown > 0)
            setEnemyCoolDown(enemyCoolDown - 1);
        else
            setEnemyCoolDown(3);
    }

    const OnHandleEnemySturn = () => {
        const sturn = EnemyGetSturn();
        setIsEnemyStunned(sturn);
    }

    const OnHandleEnemyAction = () => {
        if (!isEnemyStunned && enemyLife) {
            const damage = (enemyCoolDown)
                ? EnemyBasicAttack()
                : EnemyEspecialAttack();

            (damage < playerLife)
                ? setPlayerLife(playerLife - damage)
                : setPlayerLife(0);

            log.push(`[Turno ${turn}] Inimigo usou "Ataque ${enemyCoolDown ? 'Básico' : 'Especial'}" (-${damage} pontos de vida)`);
            //console.log(`Inimigo causou ${damage} de dano ${enemyCoolDown ? 'básico' : 'especial'}`);
            OnHandleEnemyCoolDown();
        }
        else {
            log.push(`[Turno ${turn}] Inimigo atordoado`);
            setIsEnemyStunned(false);
        }
    }

    const OnHandleEnemyDefeated = async () => {
        setEnemyLife(0)
        alert('Parabéns, você conseguiu!');
        setIsGameActive(false);
        const score = Math.round((playerLife * 1000) / turn);
        const data = { playerName, score}
        //console.log(data);

        request.post('/players', data)
            .then(res => {
                setPlayer(res.data);
            })
            .catch(err => {
                console.log(err.response.data);
            })
    }

    const OnHandlePlayerDamage = (damage: number) => {
        (damage < enemyLife)
            ? setEnemyLife(enemyLife - damage)
            : OnHandleEnemyDefeated();
        OnHandlePlayerCoolDown();
    }

    const OnHandlePlayerHeal = () => {
        const heal = PlayerHeal();
        const life = playerLife + heal;
        (life > 100)
            ? setPlayerLife(100)
            : setPlayerLife(life);
        log.push(`[Turno ${turn}] Jogador usou "Curar" (+${heal} pontos de vida)`);
        //console.log(`Jogador curou ${heal}%`);
        OnHandlePlayerCoolDown();
        OnHandleEnemyAction();
        setTurn(turn + 1);
    }

    return (
        <div className='game-page-container'>
            <div className='card-base'>
                {isGameActive ? (
                    <div className='game-active'>
                        <h5 className='text-primary'>
                            {isEnemyStunned ? (
                                'Inimigo atordoado, está é sua chance!'
                            ) : ''}
                        </h5>
                        <div className='life-point-container'>
                            <LifePointsBar name={`Jogador: ${playerName}`} life={playerLife} />
                            <LifePointsBar name='Inimigo' life={enemyLife} />
                        </div>
                        <div className='player-commands-container'>
                            <h4>
                                Comandos
                            </h4>
                            <div className='player-commands-btns'>
                                <button type='button' className='btn btn-blue' onClick={
                                    () => {
                                        const damage = PlayerBasicAttack();
                                        OnHandlePlayerDamage(damage);
                                        log.push(`[Turno ${turn}] Jogador usou "Ataque Básico" (-${damage} pontos de vida)`);
                                        //console.log(`Ataque básico causou ${damage} de dano`);
                                        OnHandleEnemyAction();
                                        setTurn(turn + 1);
                                    }
                                }>
                                    Ataque
                                </button>
                                <button
                                    type='button'
                                    className={'btn ' + (
                                        (!playerCoolDown)
                                            ? 'btn-danger'
                                            : 'btn-outline-danger'
                                    )}
                                    onClick={
                                        () => {
                                            const damage = PlayerEspecialAttack();
                                            OnHandlePlayerDamage(damage);
                                            log.push(`[Turno ${turn}] Jogador usou "Ataque Especial" (-${damage} pontos de vida)`);
                                            //console.log(`Ataque especial causou ${damage} de dano`);
                                            setPlayerCoolDown(2);
                                            OnHandleEnemyAction();
                                            OnHandleEnemySturn();
                                            setTurn(turn + 1);
                                        }
                                    }
                                    disabled={Boolean(playerCoolDown)}
                                >
                                    Ataque Especial
                                </button>
                                <button type='button' className='btn btn-primary' onClick={OnHandlePlayerHeal}>
                                    Curar
                                </button>
                                <button type='button' className='btn btn-purple' onClick={OnHandleGiveUp}>
                                    Desistir
                                </button>
                            </div>
                        </div>
                        <h4>
                            Log
                        </h4>
                        <div className='game-log'>
                            {log.slice(0).reverse().map(item => (
                                <p key={item}>
                                    {item}
                                </p>
                            ))}
                        </div>
                    </div>
                ) : (
                    <div className='game-inactive'>
                        {enemyLife
                            ? (
                                <>
                                    <h5>Escolha um nome e comece:</h5>
                                    <input
                                        type="text"
                                        value={playerName}
                                        className='form-control'
                                        placeholder='nome do jogador'
                                        onChange={item => setPlayerName(item.target.value)}
                                    />
                                    <button
                                        type='button'
                                        className={
                                            'btn ' + (
                                                (playerName)
                                                    ? 'btn-primary'
                                                    : 'btn-outline-primary'
                                            )
                                        }
                                        onClick={() => setIsGameActive(true)}
                                        disabled={!Boolean(playerName)}
                                    >
                                        iniciar jogo
                                    </button>
                                </>
                            ) : (
                                player && (
                                    <>
                                    <h1>
                                        Parabéns {player.playerName}!
                                    </h1>
                                    <h3>
                                        Data: {formatDate(player.data)}
                                    </h3>
                                    <h3>
                                        Pontuação: {player.score}
                                    </h3>
                                    <Link
                                        to='/ranking'
                                        type='button'
                                        className='btn btn-primary'
                                    >
                                        ver ranking
                                    </Link>
                                </>
                                )
                            )}
                    </div>
                )}
            </div>
        </div>
    );
}

export default GamePage;