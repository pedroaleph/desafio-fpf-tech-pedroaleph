import Pagination from 'core/components/Pagination';
import { PlayersResponse } from 'core/types/player';
import { formatDate } from 'core/utils/format';
import request from 'core/utils/request';
import { useEffect, useState } from 'react';
import './styles.scss';

const RankingPage = () => {
    const [playersResponse, setPlayersResponse] = useState<PlayersResponse>();
    const [activePage, setActivePage] = useState(0);

    useEffect(() => {
        request.get(`/players?page=${activePage}`)
            .then(res => {
                setPlayersResponse(res.data);
            })
            .catch(err => {
                console.log(err);
            })
    }, [activePage]);

    return (
        <div className='ranking-page-container'>
            <div className='card-base'>
                <table className='ranking-table'>
                    <thead>
                        <tr>
                            <th>nome</th>
                            <th>data</th>
                            <th>pontuação</th>
                        </tr>
                    </thead>
                    <tbody>
                        {playersResponse?.content.map(player => (
                            <tr key={player._id}>
                                <td>{player.playerName}</td>
                                <td>{formatDate(player.data)}</td>
                                <td>{player.score}</td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
            { playersResponse && (
                <Pagination 
                    totalPages={playersResponse.totalPages}
                    onChange={page => setActivePage(page)}
                />
            ) }
        </div>
    );
}

export default RankingPage;