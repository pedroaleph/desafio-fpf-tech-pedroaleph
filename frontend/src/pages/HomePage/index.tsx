import { Link } from 'react-router-dom';
import './styles.scss';

const HomePage = () => {
    return (
        <div className='home-page-container'>
            <div className='card-base'>
                <h1>Bem Vindo(a)!</h1>
                <h5>
                    Esta aplicação é uma a solução para o desafio prático do processo seletivo da <span>
                        <a href="https://fpftech.com" target="_blank" rel='noreferrer'>FPF TECH</a>
                    </span> e está disponivel para acesso no repositório <a
                        href="https://gitlab.com/pedroaleph/desafio-fpf-tech-pedroaleph.git"
                        target="_blank" rel='noreferrer'
                    >
                        aqui
                    </a>.
                </h5>
                <p className='home-description'>
                    Aqui você pode:
                    <br />
                        <span>
                            - Experimentar um <Link to='/game'>
                                jogo
                            </Link> em turnos
                        </span>
                    <br />
                        <span>
                            - Visualizar as pontuações dos jogadores no <Link to='/ranking'>
                                ranking
                            </Link>
                        </span>
                    <br />
                </p>
                <p className='home-developer'>
                    App desenvolvido por <a
                        href="https://gitlab.com/pedroaleph"
                        target="_blank"
                        rel="noreferrer"
                    >
                        Pedro Aleph
                    </a>
                </p>
            </div>
        </div>
    );
}

export default HomePage;