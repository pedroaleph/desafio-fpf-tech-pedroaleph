import './App.scss';
import 'core/assets/styles/custom.scss';
import Screens from 'Screens';

function App() {
  return <Screens />
}

export default App;
