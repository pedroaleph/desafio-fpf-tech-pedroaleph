<div id="top"></div>

# Desafio  FPF Tech

Solução do desafio prático do processo seletivo da fpf tech

A aplicação consiste em:
 - Jogo em turnos, onde o usuário primeiro deve inserir seu nome de jogador
 - Ranking dos jogadores com as pontuações em ordem decrescente
    (o usuário é adiciando ao ranking após vencer uma partida)

Recursos adicionais:
    - Inimigo pode ficar atortoado no turno sequinte a um ataque especial
    - Mudança da cor na barra de vida de acordo com os pontos de vida restantes
    - Log de ações do jogador e do Inimigo em cada turno em ordem decrescente

## Layout

<div align="center">
  <a href="https://gitlab.com/pedroaleph/desafio-fpf-tech-pedroaleph/-/blob/main/Layout1.png">
    <img src="Layout1.jpg" alt="Logo">
  </a>
   <a href="https://gitlab.com/pedroaleph/desafio-fpf-tech-pedroaleph/-/blob/main/Layout2.png">
    <img src="Layout2.jpg" alt="Logo">
  </a>
   <a href="https://gitlab.com/pedroaleph/desafio-fpf-tech-pedroaleph/-/blob/main/Layout3.png">
    <img src="Layout3.jpg" alt="Logo">
  </a>
</div>

## Deploy

[![Netlify Status](https://api.netlify.com/api/v1/badges/e750781a-5c42-4e19-b851-b2f275bae7f5/deploy-status)](https://app.netlify.com/sites/fpf-tech-pedroaleph/deploys)

NETLIFY: [Front end](https://fpf-tech-pedroaleph.netlify.app)

HEROKU: [Back end](https://api-fpf-tech-pedroaleph.herokuapp.com)


# Tecnologias utilizadas

## Banco de dados
- MongoDB

## Back end
- TypeScript
- NodeJS
- Express
- Ts-node
- Mongoose
- Mongoose-sequence

## Front end
- TypeScript
- ReactJS
- HTML
- React-router-dom
- Bootstrap
- Sass
- React-paginate
- Axios
- React-hook-form

# Como executar o projeto

## Utilizando Docker Compose
Pré-requisitos: [Docker Engine](https://docs.docker.com/get-docker) e [Docker Compose](https://docs.docker.com/compose/install)

```bash
# clonar repositório
git clone https://gitlab.com/pedroaleph/desafio-fpf-tech-pedroaleph.git

# Executar na pasta raiz do projeto no modo root
docker-compose up
```
Após a ativação dos containers, os subprojetos podem ser acessados localmente:

- Front End: http://localhost:3000
- Back End: http://localhost:5000

## Executando manualmente

## Back end
Pré-requisitos: npm / yarn e [MongoDB](https://docs.mongodb.com/guides/server/install)

```bash
# clonar repositório
git clone https://gitlab.com/pedroaleph/desafio-fpf-tech-pedroaleph.git

# iniciar o banco de dados no modo root
systemctl start mongod

# entrar na pasta do projeto back end
cd backend

# executar o projeto
yarn start
```

## Front end web
Pré-requisitos: npm / yarn

```bash
# clonar repositório
git clone https://gitlab.com/pedroaleph/desafio-fpf-tech-pedroaleph.git

# entrar na pasta do projeto front end web
cd frontend

# instalar dependências
yarn install

# executar o projeto
yarn start
```
# Autor

[Pedro Aleph Gomes de Souza Vasconcelos](https://www.linkedin.com/in/pedro-aleph-vasconcelos-62521a203)
