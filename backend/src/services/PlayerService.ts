import { PlayerType } from "../models/Player";

const PlayerModel = require('../models/Player');


export const findPaged = async (size: number, page: number) => {
    try {

        const total = await PlayerModel.estimatedDocumentCount();
    
        const totalPages = Math.ceil((size === 0) ? 1 : total / size);
    
        const skip = page * size;
        
        const data = await PlayerModel.find().sort({ score: -1 }).limit(size).skip(skip);

        const dataPaged = {
            content: data,
            size: size,
            page: page,
            totalPages: totalPages,
            totalElements: total
        }

        return dataPaged;
    } catch (error) {
        throw error;
    }
}

export const create = async (player: PlayerType) => {
    try {
        const data = await PlayerModel.create(player);

        return data;
    } catch (error) {
       throw error; 
    }
}