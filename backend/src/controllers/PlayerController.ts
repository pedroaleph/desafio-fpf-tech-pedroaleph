import { Request, Response } from 'express';
import { create, findPaged } from '../services/PlayerService';

export const findPagedPlayers = async (req: Request, res: Response) => {
    const intSize = Number(req.query.size);
    const intPage = Number(req.query.page);

    try {
        const size = (isNaN(intSize)) ? 8 : intSize;
        const page =  (isNaN(intPage)) ? 0 : intPage;

        const data = await findPaged(size, page);

        res.send(data);        
    } catch (error:any) {
        res.status(400).send({ success: false, message: error.message });
    }
}

export const createPlayer = async (req: Request, res: Response) => {
    const player = req.body;
    try {
        const data = await create(player);

        res.status(201).send(data);
        
    } catch (error:any) {
        res.status(400).send({ success: false, message: error.message });
    }
}