import mongoose, { model, Schema } from "mongoose";

const AutoIncrement = require('mongoose-sequence')(mongoose);

export type PlayerType = {
    _id?: number,
    playerName: string,
    score: number,
    data: Date | number
}


const PlayerSchema = new Schema<PlayerType>({
    _id: Number,
    playerName: { 
        type: String,
        required: true,
        maxlength: 15
    },
    score: {
        type: Number,
        required: true
    },
    data: {
        type: Date,
        default: Date.now()
    }
});

PlayerSchema.plugin(AutoIncrement, { id: "player_id_counter", inc_field: "_id" });
  
  
module.exports = model<PlayerType>('Players', PlayerSchema);


